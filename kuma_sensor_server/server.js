const express = require('express');
const path = require("path");
const  fs = require('fs');
const low = require('lowdb');
const FileSync = require('lowdb/adapters/FileSync');

const app = express();
// Constants
const PORT = 3000;
const HOST = '0.0.0.0';
app.use(express.json());

const retention_count = 2000;

app.get('/', (req, res) => res.sendFile(path.join(__dirname, 'home.html')));

app.get('/report/:device_id', function (req, res) {
  // db = get_db(req.params.device_id);
  res.sendFile(path.join(__dirname, 'device_report.html'));
});

app.get('/data/:device_id', function (req, res) {
  db = get_db(req.params.device_id);
  res.send(db);
});

app.post('/update/:device_id', function (req, res) {
  const data = req.body;
  db = get_db(req.params.device_id);
  // new device, set default db
  if (db.get('timestamp').value() == null) {
    db.defaults({
      timestamp: [],
      ec: [],
      ph: [],
      litre: [],
      water_temp: [],
      room_temp: [],
      room_humid: [],
      status: [],
      count: 0 
    }).write();
  }
  const count = db.get('timestamp').value().length;
  db.update('count', n => n + 1).write();

  var remove_first = (count >= retention_count) ? true : false;
  for (var key in data){
    if(['device', 'name'].indexOf(key) < 0){
      db.get(key).push(data[key]).write();
      if (remove_first)
        db.get(key).pullAt(0).write();
    } 
  }
  res.send('Update');
});

app.use('/static', express.static(path.join(__dirname, 'static')))
app.listen(PORT, HOST, () => console.log(`Running on http://${HOST}:${PORT}`));


function get_db_filename(device_id) {
  return 'db/db_' + device_id + '.json';
}

function get_db(device_id) {
  const adapter = new FileSync(get_db_filename(device_id));
  const db = low(adapter);
  return db;
}