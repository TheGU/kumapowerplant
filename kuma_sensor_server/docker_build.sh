docker stop kuma_plant_server
docker rm kuma_plant_server
docker rmi kuma_plant_server
npm install
docker build -t kuma_plant_server .
mkdir -p /app/kumaplant_db
docker run --name kuma_plant_server  -v /app/kumaplant_db:/app/db -p 3000:3000 -d kuma_plant_server