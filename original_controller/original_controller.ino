#include <Wire.h>
#include <Adafruit_ADS1015.h>

int refill_interval = 0;
float target_pH = 6.0;
float target_EC = 1.2;

#define D5 14
#define D6 12
#define D7 13

int pH_pump=D5;
int EC_pump=D6;
int water_pump=D7;

Adafruit_ADS1115 ads(0x48);  /* Use this for the 16-bit version */
//Adafruit_ADS1015 ads;     /* Use thi for the 12-bit version */

void setup(void)
{
  Serial.begin(9600);
  Serial.println("Hello!");

  Serial.println("Getting single-ended readings from AIN0..3");
  Serial.println("ADC Range: +/- 6.144V (1 bit = 3mV/ADS1015, 0.1875mV/ADS1115)");

   pinMode(D5,OUTPUT); // setup output
   digitalWrite(D5,HIGH); 

   pinMode(D6,OUTPUT); // setup output
   digitalWrite(D6,HIGH); 

   pinMode(D7,OUTPUT); // setup output
   digitalWrite(D7,HIGH); 
   
  // The ADC input range (or gain) can be changed via the following
  // functions, but be careful never to exceed VDD +0.3V max, or to
  // exceed the upper and lower limits if you adjust the input range!
  // Setting these values incorrectly may destroy your ADC!
  //                                                                ADS1015  ADS1115
  //                                                                -------  -------
  // ads.setGain(GAIN_TWOTHIRDS);  // 2/3x gain +/- 6.144V  1 bit = 3mV      0.1875mV (default)
  // ads.setGain(GAIN_ONE);        // 1x gain   +/- 4.096V  1 bit = 2mV      0.125mV
  // ads.setGain(GAIN_TWO);        // 2x gain   +/- 2.048V  1 bit = 1mV      0.0625mV
  // ads.setGain(GAIN_FOUR);       // 4x gain   +/- 1.024V  1 bit = 0.5mV    0.03125mV
  // ads.setGain(GAIN_EIGHT);      // 8x gain   +/- 0.512V  1 bit = 0.25mV   0.015625mV
  // ads.setGain(GAIN_SIXTEEN);    // 16x gain  +/- 0.256V  1 bit = 0.125mV  0.0078125mV
  ads.setGain(GAIN_ONE);
  ads.begin();
}

void pump(int pin,int cc) {
   digitalWrite(pin,LOW); // Pin D5 is LOW
   delay(cc*1121);
   digitalWrite(pin,HIGH); // Pin D5 is LOW
}

void loop(void)
{
  int16_t adc0, adc1, adc2, adc3 , results;
  float pH = 0;
  float EC = 0;

  //LOOPTEST
  /*
  adc0 = 0;
  for (int j = 0; j < 10; j++) {
    adc0 += ads.readADC_SingleEnded(0);
    delay(100);
  }
  adc0 = adc0 / 10.0;
 */
  adc0=ads.readADC_SingleEnded(0);

  adc0 = ads.readADC_SingleEnded(0);
  //  adc1 = ads.readADC_SingleEnded(1);
  //  adc2 = ads.readADC_SingleEnded(2);
  //  adc3 = ads.readADC_SingleEnded(3);
  EC = adc0 * 0.125 / 135.0;
  Serial.print("AIN0: "); Serial.print(adc0 * 0.125);  Serial.print(" EC = "); Serial.println(EC);
  //  Serial.print("AIN1: "); Serial.println(adc1*0.1875);

  /*
    results=0;
    for(int k=0;k<3;k++) {
      results += ads.readADC_Differential_2_3();
      delay(1000);
    }
    results=results/3.0;
  */
  results = ads.readADC_Differential_2_3();
  pH = ((results * 0.125) - 750.0 ) / 158.3 ;
  //  Serial.print("AIN2: "); Serial.println(adc2*0.1875);
  //  Serial.print("AIN3: "); Serial.println(adc3*0.1875);
  Serial.print("diff_2_3: "); Serial.print(results * 0.125); Serial.print(" pH = "); Serial.println(pH);
  Serial.println(" ");
  if (refill_interval == 0) {
    if (pH > target_pH+0.1) {
      Serial.println("Pump! for pH down!!");
      if (target_pH - pH > 0.5) {
        pump(pH_pump,2);
      } else {
        pump(pH_pump,1);
      }
      Serial.println("Pump off!");
    }
    if (EC < target_EC) {
      Serial.println("Pump! for EC up!!");
      if (EC < target_EC - 0.5) {
        pump(EC_pump,2);
      } else {
        pump(EC_pump,1);
      }
      Serial.println("Pump off!");
    } else if (EC > target_EC + 0.2) {
      Serial.println("Pump water for EC down!!");
      pump(water_pump,2); 
      Serial.println("Pump off!");
    }
  } 
  refill_interval++;
  if (refill_interval == 30) refill_interval = 0;
  delay(5000);
}