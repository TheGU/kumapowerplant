#include "EmonLib.h"
#include <Wire.h>
#include <LiquidCrystal_I2C.h>

#define HOME_VOLTAGE 240.0
// The GPIO pin were the CT sensor is connected to (should be an ADC input)
#define ADC_INPUT A0

// Force EmonLib to use 10bit ADC resolution
#define ADC_BITS    10
#define ADC_COUNTS  (1<<ADC_BITS)

LiquidCrystal_I2C lcd(0x27, 16, 2);

EnergyMonitor emon1;
unsigned long lastMeasurement = millis();

void setup() 
{
  Serial.begin(115200);
  emon1.current(ADC_INPUT, 31.24);   // Current: input pin, calibration.

  lcd.begin();
  lcd.backlight();
  lcd.setCursor(0, 0);
  lcd.print("Initialize");
  lcd.setCursor(2, 1);
  lcd.print("PATTAPONG J.");
  lcd.clear();
}
 void loop() 
{
  unsigned long currentMillis = millis();
  if(currentMillis - lastMeasurement > 1000){
    double amps = emon1.calcIrms(1480); // Calculate Irms only
    double watt = amps * HOME_VOLTAGE;
    lcd.setCursor(0, 0);
    lcd.print("AMPS    WATT");
    lcd.setCursor(0, 1);
    lcd.print(amps);
    lcd.setCursor(8, 1);
    lcd.print(watt,2);
    
    Serial.print("Amps = ");
    Serial.print(amps);
    Serial.print(" , Watt = ");
    Serial.print(watt,2);
    Serial.println("");
    
    lastMeasurement = millis();
  }
}
