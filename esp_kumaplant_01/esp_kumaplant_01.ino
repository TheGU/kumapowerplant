
#include <ESP8266WiFi.h> //https://github.com/esp8266/Arduino

#include <time.h>

#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <ESP8266mDNS.h>;
#include <ESP8266HTTPClient.h>
#include <WiFiManager.h> //https://github.com/tzapu/WiFiManager
#include <WiFiClientSecureBearSSL.h>

#include <OneWire.h>           //http://www.pjrc.com/teensy/td_libs_OneWire.html
#include <DallasTemperature.h> //https://github.com/milesburton/Arduino-Temperature-Control-Library

#include <Wire.h>
#include <Adafruit_ADS1015.h>

// MUST DEFINE
// TODO: Interface to change name
const String DEVICE_NAME = "kuma1";
// TODO: Write custom ID to eprom. Not sure chip id is unique or not
const String DEVICE_ID = String(ESP.getChipId());

// Web Server
const String WebURL="https://plant.kuma-power.com";
const String WebUpdateURL="https://plant.kuma-power.com/update/" + DEVICE_ID;
const int WebUpdateInteval=300;

// Pump Variable
unsigned long g_sensor_lastupdate = 0; // Timer helper
unsigned long g_web_lastupdate = 0; // Timer helper
int refill_interval = 0;

//NEW
#define D1 5
#define D2 4
#define D3 0
#define D4 2
#define D5 14
#define D6 12
#define D7 13
#define D8 15

/*
#define D5 14
#define D6 12
#define D7 13
#define D8 15
#define D4 2
#define D1 5
*/
#define ONE_WIRE_BUS D1

/*PUMP*/
int PUMP1 = D3;
int PUMP2 = D4;
int PUMP3 = D5;
int PUMP4 = D6;
int PUMP5 = D7;
int PUMP6 = D8;


int pH_pump_down = PUMP1;
int pH_pump_up = D7; // NOT USE NOW
int EC_pump_A = PUMP3;
int EC_pump_B = PUMP2;
int water_pump = D7; // NOT USE NOW

int must_pump_B = 0;

// Global Variable
float global_EC = 0;
float global_pH = 7;
int global_Litre = 10;
int global_Sol = 10;
float global_water_temperature = 32;
float global_room_temperature = 28;
float global_room_humidity = 60;
float raw_pH=0;
float raw_EC=0;

float global_target_EC = 0;
float global_target_pH = 0;

int wait_for_AB = 0;
int wait_for_BA = 0;
int ec_fill_A = 0;
int ec_fill_B = 0;
int ec_next_check = 0;

// Internal Variable
int timezone = 7 * 3600; // GMT + 7

// Setup a oneWire instance to communicate with any OneWire devices (not just Maxim/Dallas temperature ICs)
OneWire oneWire(ONE_WIRE_BUS);

// Pass our oneWire reference to Dallas Temperature.
DallasTemperature temp_dallas(&oneWire);

// Class Init
Adafruit_ADS1115 ads(0x48); /* Use this for the 16-bit version */
//Adafruit_ADS1015 ads;     /* Use thi for the 12-bit version */
ESP8266WebServer server(80);

void setup()
{
  pinMode(D3, OUTPUT); // setup output
  digitalWrite(D3, LOW);
  pinMode(D4, OUTPUT); // setup output
  digitalWrite(D4, LOW);
  pinMode(D5, OUTPUT); // setup output
  digitalWrite(D5, LOW);
  pinMode(D6, OUTPUT); // setup output
  digitalWrite(D6, LOW);
  pinMode(D7, OUTPUT); // setup output
  digitalWrite(D7, LOW);
  pinMode(D8, OUTPUT); // setup output
  digitalWrite(D8, LOW);
  
  Serial.begin(115200);

  // Wifi Setup =====
  WiFiManager wifiManager;
  //reset saved settings
  //wifiManager.resetSettings();

  //sets timeout until configuration portal gets turned off
  //useful to make it all retry or go to sleep
  //in seconds
  wifiManager.setTimeout(30);
  wifiManager.setConfigPortalTimeout(30);

  // wifiManager.autoConnect("@KumaPower_Setup");
  if (!wifiManager.autoConnect(strcat("@KumaPower_Setup_", DEVICE_NAME.c_str())))
  {
    Serial.println("failed to connect and hit timeout");
    //reset and try again, or maybe put it to deep sleep
    // wifiManager.resetSettings();
    ESP.reset();
    delay(5000);
  }
  Serial.println("connected... :)");

  // Set domain name
  if (!MDNS.begin(DEVICE_NAME.c_str()))
  {
    Serial.println("Error setting up MDNS responder!");
  }
  else
  {
    Serial.println("mDNS responder started");
  }

  // Run Web Server =====
  server.on("/", handle_status);
  server.on("/update", handle_update);
  server.on("/reset", handle_Reset);
  server.onNotFound(handle_NotFound);

  server.begin();
  Serial.println("HTTP server started");

  // DateTime initial =====
  configTime(timezone, 0, "pool.ntp.org", "time.nist.gov");
  Serial.println("\nWaiting for time");
  while (!time(nullptr))
  {
    Serial.print(".");
    delay(1000);
  }
  // Temp Sensor initail
  temp_dallas.begin();

  // Sensor initial =====
  Serial.println("Getting single-ended readings from AIN0..3");
  Serial.println("ADC Range: +/- 6.144V (1 bit = 3mV/ADS1015, 0.1875mV/ADS1115)");
  

  // The ADC input range (or gain) can be changed via the following
  // functions, but be careful never to exceed VDD +0.3V max, or to
  // exceed the upper and lower limits if you adjust the input range!
  // Setting these values incorrectly may destroy your ADC!
  //                                                                ADS1015  ADS1115
  //                                                                -------  -------
  // ads.setGain(GAIN_TWOTHIRDS);  // 2/3x gain +/- 6.144V  1 bit = 3mV      0.1875mV (default)
  // ads.setGain(GAIN_ONE);        // 1x gain   +/- 4.096V  1 bit = 2mV      0.125mV
  // ads.setGain(GAIN_TWO);        // 2x gain   +/- 2.048V  1 bit = 1mV      0.0625mV
  // ads.setGain(GAIN_FOUR);       // 4x gain   +/- 1.024V  1 bit = 0.5mV    0.03125mV
  // ads.setGain(GAIN_EIGHT);      // 8x gain   +/- 0.512V  1 bit = 0.25mV   0.015625mV
  // ads.setGain(GAIN_SIXTEEN);    // 16x gain  +/- 0.256V  1 bit = 0.125mV  0.0078125mV
  ads.setGain(GAIN_ONE);
  ads.begin();
}

void loop()
{
  // Get current time
  time_t now = time(nullptr);
  struct tm *p_tm = localtime(&now);

  // Read sensor every X seconds
  if (g_sensor_lastupdate + 6000 <= millis())
  {
    sensor_test();
    g_sensor_lastupdate = millis();
  }

  // Update to web every X seconds
  if (g_web_lastupdate + WebUpdateInteval*1000 <= millis())
  {
    httpSendSensorStatus("");
    g_web_lastupdate = millis();
  }

  // Handle Web Request
  server.handleClient();
}

// Update status helper
void httpSendSensorStatus(String optional_text) {
  std::unique_ptr<BearSSL::WiFiClientSecure>client(new BearSSL::WiFiClientSecure);
  client->setInsecure();
  
  HTTPClient https;  //Declare an object of class HTTPClient
  String msg = "{";
  msg += "\"device\":\"" + DEVICE_ID + "\",";
  msg += "\"name\":\"" + DEVICE_NAME + "\",";
  msg += "\"status\":\"" + optional_text + "\",";
  msg += "\"ec\":" + String(global_EC, 2) + ",";
  msg += "\"ph\":" + String(global_pH, 2) + ",";
  msg += "\"litre\":" + String(global_Litre) + ",";
  msg += "\"water_temp\":" + String(global_water_temperature, 2) + ",";
  msg += "\"room_temp\":" + String(global_room_temperature, 2) + ",";
  msg += "\"room_humid\":" + String(global_room_humidity, 2) + ",";
  msg += "\"timestamp\":\"" + _currentTimeStr() + "\"}";

  Serial.println("Send update to server");
  Serial.println(msg);
  https.begin(*client, WebUpdateURL);
  https.addHeader("Content-Type", "application/json");
  int httpCode = https.POST(msg); //Send the request
//  Serial.println(WebUpdateURL);
   Serial.println(httpCode);
    // httpCode will be negative on error
    if (httpCode > 0) {
      // HTTP header has been send and Server response header has been handled
      Serial.printf("[HTTP] POST... code: %d\n", httpCode);
      // file found at server
      if (httpCode == HTTP_CODE_OK) {
        const String& payload = https.getString();
        Serial.println(payload);
      }
    } else {
      Serial.printf("[HTTP] POST... failed, error: %s\n", https.errorToString(httpCode).c_str());
    }
  https.end();   //Close connection
}


// Pump Function =====
void pump(int pin, int cc)
{
  digitalWrite(pin, LOW); // Pin D5 is LOW
  delay(cc * 1121);
  digitalWrite(pin, HIGH); // Pin D5 is LOW
  httpSendSensorStatus("Pump pin(" + String(pin) + ") cc(" + String(cc) + ")");
}

void sensor_test()
{
  int16_t adc0, adc1, adc2, adc3, results;
  float pH = 0;
  float EC = 0;

  adc0 = ads.readADC_SingleEnded(0);
  raw_EC = adc0;
 // EC = adc0 * 0.125 / 135.0;
  EC = (adc0 - 55.696) / 816;
  global_EC = EC;
  // Serial.print("AIN0: "); Serial.print(adc0 * 0.125);  Serial.print(" EC = "); Serial.println(EC);

  results = ads.readADC_Differential_2_3();
  raw_pH = results;
  //pH = ((results * 0.125) - 750.0) / 158.3;
  pH = ((results * 0.125) - 628.26) / 170.5;
  global_pH = pH;
  // Serial.print("diff_2_3: "); Serial.print(results * 0.125); Serial.print(" pH = "); Serial.println(pH);
  // Serial.println(" ");

  //read temp
  temp_dallas.requestTemperatures();
  float temp1 = temp_dallas.getTempCByIndex(0);

  if (refill_interval == 0 && global_target_EC != 0)
  {
    if (pH > 2 && pH < 11)
    { //Protect from unusual pH meter failure
      if (pH > global_target_pH + 0.2)
      {
        Serial.println("Pump! for pH down!!");
        if (global_target_pH - pH > 0.5)
        {
          pump(pH_pump_down, 2);
        }
        else
        {
          pump(pH_pump_down, 1);
        }
        Serial.println("Pump off!");
      }
      else if (pH < global_target_pH - 0.2)
      {
        Serial.println("Pump! for pH up!!");
        if (global_target_pH - pH > 0.5)
        {
          pump(pH_pump_up, 2);
        }
        else
        {
          pump(pH_pump_up, 1);
        }
        Serial.println("Pump off!");
      }
    }

    if (EC < 5 && EC > 0.05)
    { //Protect from unusual EC meter failure
      
      if (EC < global_target_EC && ec_next_check < millis())
      {
        ec_fill_A = 1;
        ec_fill_B = 1;  
      }
      else if (EC > global_target_EC + 0.2)
      {
        Serial.println("Pump water for EC down!!");
        //pump(water_pump, 2);
        Serial.println("Pump off!");
      }

      if (ec_fill_A > 0) {
        pump(EC_pump_A, 5);
        ec_fill_A = ec_fill_A - 1;
        wait_for_AB = millis()+5000;
        ec_next_check = millis()+30000;
      } else if (ec_fill_B > 0 && wait_for_AB < millis()) {
        pump(EC_pump_B, 5);
        ec_fill_B = ec_fill_B - 1;
        ec_next_check = millis()+30000;
      }
            
    }
  }
  refill_interval++;
  if (refill_interval == 1)
    refill_interval = 0;
}

// Web Function =====
void handle_status()
{
  server.send(200, "text/html", SendHTML());
}

void handle_update()
{
  String t_ec = server.arg("t_ec");
  String t_ph = server.arg("t_ph");
  String t_litre = server.arg("t_litre");
  global_target_EC = t_ec.toFloat();
  global_target_pH = t_ph.toFloat();
  global_Litre = t_litre.toInt();
  // global_water_temperature
  // global_room_temperature
  // global_room_humidity

  // Serial.println("EC[" + t_ec + "] pH[" + t_ph + "] litre[" + t_litre + "]");
  server.send(200, "text/html", SendHTML());
}

void handle_NotFound()
{
  server.send(404, "text/plain", "Not found");
}

void handle_Reset()
{
  ESP.reset();
  server.send(200, "text/plain", "Restarting");
}

// HTML Template =======================================
String _currentTimeStr()
{
  // Get current time
  time_t now = time(nullptr);
  struct tm *p_tm = localtime(&now);
  char dateStr[12];
  sprintf(dateStr, "%4d-%02d-%02d", p_tm->tm_year + 1900, p_tm->tm_mon + 1, p_tm->tm_mday);
  char timeStr[10];
  sprintf(timeStr, "%02d:%02d:%02d", p_tm->tm_hour, p_tm->tm_min, p_tm->tm_sec);
  return String(dateStr) + " " + String(timeStr);
}

String _HTMLPageHeader(String title_page, String detail_page)
{
  String data = "<div class=\"container\">";
  data += "  <div class=\"py-3 text-center\">";
  data += "    <h2>" + title_page + "</h2>";
  data += "    <p class=\"lead\">" + detail_page + "</p>";
  data += "  </div>";
  data += "<div class=\"row\">";
  return data;
}

String _HTMLPageFooter(String title_footer)
{
  String data = "</div>";
  data += "  <footer class=\"my-5 pt-5 text-muted text-center text-small\">";
  data += "    <p class=\"mb-1\">&copy; " + title_footer + "</p>";
  data += "    <ul class=\"list-inline\">";
  data += "      <li class=\"list-inline-item\"><a href=\"/reset\">Reset Device</a></li>";
  data += "    </ul>";
  data += "  </footer>";
  data += "</div>";
  return data;
}

String _HTMLHeader(String title_web)
{
  String data = "<!doctype html><html lang=\"en\"><head><meta charset=\"utf-8\">";
  data += "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">";
  data += "<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css\">";
  data += "<title>" + title_web + "</title>";
  data += "</head><body class=\"bg-light\">";
  return data;
}

String _HTMLFooter()
{
  String data = "";
  data += "<script src=\"https://code.jquery.com/jquery-3.3.1.slim.min.js\"></script>";
  data += "<script src=\"https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js\"></script>";
  data += "<script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js\"></script>";
  data += "</body></html>";
  return data;
}

String _HTMLFormNumber(String label, String name, String value, String step = "1", int block_size = 4)
{
  String data = "<div class=\"col-sm-" + String(block_size) + " mb-3\">";
  data += "<label for=\"" + name + "\">" + label + "</label>";
  data += "  <input type=\"number\" class=\"form-control\"";
  data += "     name=\"" + name + "\"";
  data += "     id=\"" + name + "\"";
  data += "     placeholder=\"\"";
  data += "     value=\"" + value + "\"";
  data += "     step=\"" + step + "\"";
  data += " required>";
  data += "</div>";
  return data;
}

String _HTMLStatus(String label, String value, String optional_text = "")
{
  String data = "<li class=\"list-group-item d-flex justify-content-between lh-condensed\">";
  data += "<span>";
  data += label;
  if (optional_text != "")
  {
    data += "<br><small class=\"text-muted\">" + optional_text + "</small>";
  }
  data += "</span>";
  data += "<strong>" + value + "</strong></li>";
  return data;
}

String SendHTML()
{
  // Page Header
  String data = _HTMLHeader("KumaPower Automation");
  data += _HTMLPageHeader("Plant Automation", "Current Time : " + _currentTimeStr());

  // Latest Status
  data += "<div class=\"col-md-6 order-md-1\"><h4 class=\"d-flex justify-content-between align-items-center mb-3\">";
  data += "  <span class=\"text-muted\">Latest Status</span>";
  data += "  <a href=\"javascript:window.location.reload(true)\" class=\"btn btn-outline-primary btn-sm\">Refresh</a>";
  data += "</h4><ul class=\"list-group mb-3\">";
  data += _HTMLStatus("Acidity Level (pH)", String(global_pH, 2), "Optimal: 5.5 - 6.5, Target: " + String(global_target_pH, 2));
  data += _HTMLStatus("Nutrient Concentration (EC)", String(global_EC, 2), "Target : " + String(global_target_EC, 2) + " millisiemens/centimeter");
  data += _HTMLStatus("Water Capacity", String(global_Litre) + " Litres", "");
  data += _HTMLStatus("Water Temperatures", String(global_water_temperature, 1) + " &#8451;", "");
  data += _HTMLStatus("Room Temperatures", String(global_room_temperature, 1) + " &#8451;", "");
  data += _HTMLStatus("Room Humidity", String(global_room_humidity) + " %", "");
  data += "</ul><span>History: <a href=\"https://plant.kuma-power.com/report/" + DEVICE_ID + "\">https://plant.kuma-power.com/report/" + DEVICE_ID + "</a></span><hr class=\"mb-4\"></div>";

  // Update Target form
  data += "<div class=\"col-md-6 order-md-2\"><h4 class=\"mb-3\">Setup Target Value</h4>";
  data += "<form action=\"/update\" method=\"get\"><div class=\"row\">";
  data += _HTMLFormNumber("Target pH", "t_ph", String(global_target_pH, 2), "0.01");
  data += _HTMLFormNumber("Target EC", "t_ec", String(global_target_EC, 2), "0.01");
  data += _HTMLFormNumber("Sol. Capacity", "t_litre", String(global_Litre), "0.5");
  data += "</div><button class=\"btn btn-primary btn-lg btn-block\" type=\"submit\">Update Setting</button></form></div>";

  // Page Footer
  data += _HTMLPageFooter("KumaPower Automation");
  data += _HTMLFooter();

  return data;
}
